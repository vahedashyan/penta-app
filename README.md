# Welcome to penta-app


## Installation

    pip install -r requirements.txt


### Performance

In the following a couple of solvers for pentadiagonal systems are compared:

* Solver 1: Standard linear algebra solver of Numpy [``np.linalg.solve``](https://www.numpy.org/devdocs/reference/generated/numpy.linalg.solve.html)
* Solver 2: [``scipy.sparse.linalg.spsolve``](http://scipy.github.io/devdocs/generated/scipy.sparse.linalg.spsolve.html)
* Solver 3: Scipy banded solver [``scipy.linalg.solve_banded``](scipy.github.io/devdocs/generated/scipy.linalg.solve_banded.html)
* Solver 4: pentapy.solve with ``solver=1``
* Solver 5: pentapy.solve with ``solver=2``

<p align="center">
<img src="https://raw.githubusercontent.com/GeoStat-Framework/pentapy/master/examples/perfplot_simple.png" alt="Performance" width="600px"/>
</p>

The implementations of pentapy are almost one order of magnitude faster than the
scipy algorithms for banded or sparse matrices.

The performance plot was created with [``perfplot``](https://github.com/nschloe/perfplot).
Have a look at the script: [``examples/03_perform_simple.py``](https://github.com/GeoStat-Framework/pentapy/blob/master/examples/03_perform_simple.py).



## Requirements:

- [NumPy >= 1.14.5](https://www.numpy.org)

### Optional

- [SciPy](https://www.scipy.org/)
- [scikit-umfpack](https://github.com/scikit-umfpack/scikit-umfpack)

## Contact

You can contact us via <sahakyandavo35@gmail.com>.


## License

[MIT][licence_link] © 2019 - 2020
